window.addEventListener("load", function () {
  const hours = document.getElementById("hours");
  const minutes = document.getElementById("minutes");
  const seconds = document.getElementById("seconds");
  const start = document.getElementById("start");
  const stop = document.getElementById("stop");
  const reset = document.getElementById("reset");

  let setHours = hours.innerHTML;
  let setMinutes = minutes.innerHTML;
  let setSeconds = seconds.innerHTML;

  start.addEventListener("click", function () {
      let interval = setInterval(() => {
        setSeconds--;
        seconds.innerHTML = setSeconds;
        if (setSeconds == 0){
          setMinutes -= 1;
          minutes.innerHTML = setMinutes;
          setSeconds = 60;
        }
        if (setMinutes == 0){
          setHours -= 1;
          hours.innerHTML = setHours;
          setMinutes = 60;
        }
        if (setSeconds == 0 && setHours == 0 && setMinutes == 0) {
          clearInterval(interval);
        }
    }, 1000);
    stop.addEventListener('click', function(){
        clearInterval(interval);
    })
});
  reset.addEventListener('click', function(){
    hours.innerHTML = 23;
    minutes.innerHTML = 59;
    seconds.innerHTML = 59;
  })
});
